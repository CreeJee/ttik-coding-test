type MergeMethod<T> = (origin: T, mergedData: T) => T;
/**
 * origin Accrulate to mergedData
 *
 * origin기준으로 합쳐짐
 * @param origin
 * @param mergedData
 * @param init
 * @param merge
 */
export const merge = <T>(
    origin: T[],
    mergedData: T[],
    init: T,
    {
        sum,
        merge,
    }: {
        sum: MergeMethod<T>;
        merge: MergeMethod<T>;
    }
): T => {
    return origin.reduce(
        (accr, value, nth) => merge(accr, sum(value, mergedData[nth])),
        init
    );
};
