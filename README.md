# API

## /registrationNumber/:number

> number: number

### respose

-   success
    -   status: 200
    -   {};
-   error
    -   status:505
        -   {error: 'NOT_ACCEPTED_INPUT'}
        -   {error: 'NOT_VAILD_INPUT'}
    -   status: 400
        -   {}

## /realtime

-   success

    -   status: 200
        -   ```javascript
            [
                {
                    "rank": 1,
                    "keyword": "초등학교 방학숙제로 미래에는",
                    "keyword_synonyms": []
                },
                ...
            ]
            ```

-   error
    -   status: 408
        -   ```javascript
            [];
            ```
