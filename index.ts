import Fastify from "fastify";

import { SERVER_PORT } from "./config";
import { realTimeNaver, registrationNumber } from "./routes/index";
const server: Fastify.FastifyInstance = Fastify();

server.register(realTimeNaver);
server.register(registrationNumber);

server.get("/", (req, res) => {
    res.send("hello world!");
});
server.listen(SERVER_PORT, function (err) {
    if (err) {
        server.log.error(err);
    }
    server.log.info(`server listening on ${SERVER_PORT}`);
});
