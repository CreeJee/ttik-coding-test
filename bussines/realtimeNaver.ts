import bent from "bent";
type PrependNextNum<A extends Array<unknown>> = A["length"] extends infer T
    ? ((t: T, ...a: A) => void) extends (...x: infer X) => void
        ? X
        : never
    : never;
type EnumerateInternal<A extends Array<unknown>, N extends number> = {
    0: A;
    1: EnumerateInternal<PrependNextNum<A>, N>;
}[N extends A["length"] ? 0 : 1];
export type Enumerate<N extends number> = EnumerateInternal<
    [],
    N
> extends (infer E)[]
    ? E
    : never;
export type Range<FROM extends number, TO extends number> = Exclude<
    Enumerate<TO>,
    Enumerate<FROM>
>;

type KeywordScore = Range<0, 5>;
type KeywordOptions = {
    entertainment: KeywordScore;
    marketing: KeywordScore;
    news: KeywordScore;
    sports: KeywordScore;
};
type RankData = {
    rank: number;
    keyword: string;
    keyword_synonyms: string[];
};
type RankResponse = {
    ts: string; //TIMESTEMP
    sm: string; //serialized-param
    rop: [
        {
            ag: string;
            gr: string;
            ma: string;
            si: string;
            en: string;
            sp: string;
        }
    ];
    data: RankData[];
};

const generateURL = (opts: KeywordOptions) => {
    return `https://apis.naver.com/mobile_main/srchrank/srchrank?frm=main&ag=20s&gr=0&ma=${
        opts.marketing - 2
    }&si=${opts.news - 2}&en=${opts.entertainment - 2}&sp=${opts.sports - 2}`;
};
const getJSON = bent("json");
export const defaultOptions: KeywordOptions = {
    entertainment: 0,
    marketing: 0,
    news: 0,
    sports: 0,
};
export async function logic(opts: KeywordOptions = defaultOptions) {
    const { data }: RankResponse = (await getJSON(
        generateURL(opts)
    )) as RankResponse;
    return data;
}
