import { merge } from "@lib/merge";
type ExpectMethod = (input: string) => boolean;

export enum ErrorState {
    //입력부족 및 누락
    NOT_ACCEPTED_INPUT = "NOT_ACCEPTED_INPUT",
    //입력이 VAILD하지않음
    NOT_VAILD_INPUT = "NOT_VAILD_INPUT",
}
const __merge = (o1: number, o2: number) => o1 + o2;
const __sum = (o1: number, o2: number) => o1 * o2;
const mergeNumber = (origin: number[], mergerData: number[]) =>
    merge<number>(origin, mergerData, 0, {
        sum: __sum,
        merge: __merge,
    });
const mapToNumber = (map: string[]) => map.map((data) => parseInt(data, 10));

const CONSTANT_MAGIC_NUMBER = [2, 3, 4, 5, 6, 7, 8, 9, 2, 3, 4, 5];

export function expectInput(input: string) {
    return /[0-9]{6}-[0-9]{7}/.exec(input) !== null;
}
export function vaildInput(input: string, __isExpected = false) {
    const numberData = mapToNumber(input.replace("-", "").split(""));
    if (__isExpected || expectInput(input)) {
        const sum = mergeNumber(CONSTANT_MAGIC_NUMBER, numberData);
        return 11 - (sum % 11) === numberData[numberData.length - 1];
    }
    return false;
}
export async function logic(input: string) {
    if (!expectInput(input)) {
        return Promise.reject(ErrorState.NOT_ACCEPTED_INPUT);
    }
    if (!vaildInput(input, true)) {
        return Promise.reject(ErrorState.NOT_VAILD_INPUT);
    }
    return true;
}
