import { logic, ErrorState } from "@bussines/registrationNumber";
import getResult from "./lib/getResult";
describe("basic-test", () => {
    test("NOT_VAILD_INPUT", async (done) => {
        expect(await getResult(logic("101010-2020200"))).toBe(
            ErrorState.NOT_VAILD_INPUT
        );
        done();
    });
    test("NOT_ACCEPTED_INPUT", async (done) => {
        expect(await getResult(logic("101010-202020"))).toBe(
            ErrorState.NOT_ACCEPTED_INPUT
        );
        done();
    });
    test("NOT_ACCEPTED_INPUT", async (done) => {
        expect(await getResult(logic(""))).toBe(ErrorState.NOT_ACCEPTED_INPUT);
        done();
    });
});
