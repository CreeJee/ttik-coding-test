export default async (promise: Promise<any>) => {
    try {
        return await promise;
    } catch (e) {
        return e;
    }
};
