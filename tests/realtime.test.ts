import { logic } from "@bussines/realtimeNaver";
import getResult from "./lib/getResult";

describe("basic-test", () => {
    test("just-correct", async (next) => {
        const data = await logic();
        expect(data.length).toBe(20);
        next();
    });
});
