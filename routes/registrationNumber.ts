import fp from "fastify-plugin";
import { logic, ErrorState } from "@bussines/registrationNumber";
export default fp(async (server, opts, next) => {
    server.get("/registrationNumber/:number", async (req, res) => {
        const input: string = "".concat(req.params.number);
        try {
            await logic(input);
            res.status(200).send({});
        } catch (e) {
            switch (e) {
                case ErrorState.NOT_ACCEPTED_INPUT:
                case ErrorState.NOT_VAILD_INPUT:
                    res.status(505).send({ error: e });
                    break;
                default:
                    res.status(400);
            }
        }
    });
});
