import registrationNumberRoute from "./registrationNumber";
import realTimeNaverRoute from "./realtimeNaver";

export const registrationNumber = registrationNumberRoute;
export const realTimeNaver = realTimeNaverRoute;

export default {
    registrationNumber,
    realTimeNaver,
};
