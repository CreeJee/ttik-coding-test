import fp from "fastify-plugin";
import { logic } from "@bussines/realtimeNaver";
export default fp(async (server, opts, next) => {
    server.get("/realtime", async (req, res) => {
        let response = {};
        try {
            response = await logic();
        } catch (e) {
            res.status(408);
        }
        res.send(response);
    });
});
