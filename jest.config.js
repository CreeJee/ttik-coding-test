module.exports = {
    transform: { "^.+\\.ts?$": "ts-jest" },
    testEnvironment: "node",
    testRegex: "/tests/.*\\.(test|spec)?\\.(ts|tsx)$",
    moduleFileExtensions: ["ts", "tsx", "js", "jsx", "json", "node"],
    moduleNameMapper: {
        "@routes/(.*)": "<rootDir>/routes/$1",
        "@lib/(.*)": "<rootDir>/lib/$1",
        "@bussines/(.*)": "<rootDir>/bussines/$1",
    },
};
